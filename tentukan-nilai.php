<?php
function tentukan_nilai($number)
{
	if($number >= 85 && $number <= 100){
		return "Sangat Baik";
	};
	if($number >= 70 && $number < 85){
		return "Baik";
	};
	if($number >= 60 && $number < 70){
		return "Cukup";
	};
	if($number < 60){
		return "Kurang";
	};

    //  kode disini
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>" . tentukan_nilai(76); //Baik
echo "<br>" . tentukan_nilai(67); //Cukup
echo "<br>" . tentukan_nilai(43); //Kurang
?>